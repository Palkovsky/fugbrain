module Main where

import Lib (VMConfig, runVM, brainfuck, makeVM)

main :: IO ()
main = do
    let printDollar = "++++++++++++++++++++++++++++++++++++."
        helloWorld1 = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."
        helloWorld2 = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."
        helloWorld3 = ">++++++++[-<+++++++++>]<.>>+>-[+]++>++>+++[>[->+++<<+++>]<<]>-----.>->+++..+++.>-.<<+[>[+>+]>>]<--------------.>>.+++.------.--------.>+.>+."
        helloWorld4 = "+[-->-[>>+>-----<<]<--<---]>-.>>>+.>>..+++[.>]<<<<.+++.------.<<-.>>>>+."
        helloWorld5 = "--<-<<+[+[<+>--->->->-<<<]>]<<--.<++++++.<<-..<<.<+.>>.>>.<<<.+++.>>.>>-.<<<+."

    putStrLn "---------OUTPUT---------"
    result <- runVM (makeVM 32768 16 helloWorld5) brainfuck
    putStrLn $ case result of
        Right output -> output
        Left _ -> "EXECUTION ERROR"

    putStrLn "---------SUMMARY---------"
    case result of
        Right _  -> putStrLn "Execution successful!"
        Left err -> putStrLn $ show err
