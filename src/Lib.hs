module Lib
    ( RuntimeException,
      VMConfig,
      brainfuck,
      makeVM,
      runVM
    ) where

import Data.Char (ord, chr)
import Data.Int
import qualified Data.Vector.Unboxed as VU
import qualified Data.Vector as VB

import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Except

type Offset = Int
data Instruction = MoveRight | MoveLeft | Increment | Decrement | Output | Input | JumpForward Offset | JumpBackward Offset | EOF | UnknownInstruction Char 
                   deriving (Show, Eq)
data RuntimeException = RuntimeException String deriving (Show)

type Code = VB.Vector Instruction
type Capacity = Int
data VMConfig = VMConfig {getRamCapacity :: Capacity, getStackCapacity :: Capacity, getInstructions :: Code}

type Memory = VU.Vector Int8
type Stack = [Int]
type OutputBuffer = String
type Ptr = Int

data VMState = VMState {getMemory :: Memory, getStack :: Stack, getInstructionPtr :: Ptr, getMemoryPtr :: Ptr, getOutputBuffer :: OutputBuffer}
type VM a = ReaderT VMConfig (StateT VMState (ExceptT RuntimeException IO)) a

-- |Gets configuration of VM.
vmConf :: VM VMConfig
vmConf = ask

-- |Gets VM state.
vmState :: VM VMState
vmState = lift get

-- |Sets new state of virtual machine.
setVM :: VMState -> VM ()
setVM = lift . put

-- |Crashes virtual machine with given message.
crash :: String -> VM a
crash msg = do
    vm <- vmState
    let instructionPtr = show $ getInstructionPtr vm
        memoryPtr = show $ getMemoryPtr vm
        message = msg ++ " Instruction pointer: " ++ instructionPtr ++ ". Memory pointer: " ++ memoryPtr
    throwError $ RuntimeException message

-- |Pushes value on stack.
push :: Int -> VM ()
push x = do
    conf <- vmConf
    vm <- vmState
    let stack    = getStack vm
        stackMax = getRamCapacity conf
    if (length stack) == stackMax then crash "Stack overflow." else  setVM (vm {getStack = x:stack})

-- |Pops value from stack.
pop :: VM Int
pop = do
    vm <- vmState
    let stack = getStack vm
    if null stack then crash "Stack underflow." else return ()
    setVM (vm {getStack = tail stack})
    return $ head stack

-- |Sets value on specified memory region.
setMem :: Ptr -> Int8 -> VM ()
setMem ptr val = do
    conf <- vmConf
    vm <- vmState
    let memory = getMemory vm
        memoryCapacity = getRamCapacity conf
    if ptr < 0 || ptr >= memoryCapacity then crash ("Tried to access out of memory region at " ++ show ptr ++ ".") else return ()
    let updated = (VU.//) memory [(ptr, val)] --(take ptr memory) ++ [val] ++ (drop (ptr + 1) memory) 
    setVM (vm{getMemory=updated}) 

setCurrentMem :: Int8 -> VM ()
setCurrentMem val = vmState >>= \vm -> setMem (getMemoryPtr vm) val

-- |Gets memory value stored at given addres.
getMem :: Ptr -> VM Int8
getMem ptr = do
    conf <- vmConf
    vm <- vmState
    let memory = getMemory vm
        memoryCapacity = getRamCapacity conf
    if ptr < 0 || ptr >= memoryCapacity then crash ("Tried to access out of memory region at " ++ show ptr ++ ".") else return (memory VU.! ptr)

-- |Gets memory value stored under current memory address.
getCurrentMem :: VM Int8
getCurrentMem = vmState >>= \vm -> getMem $ getMemoryPtr vm 

-- |Changes memory pointer to passed address.
moveTo :: Ptr -> VM ()
moveTo ptr = do
    conf <- vmConf
    if ptr < 0 || ptr >= (getRamCapacity conf) then crash ("Tried to access out of memory region at " ++ show ptr ++ ".") 
    else vmState >>= \vm -> setVM (vm{getMemoryPtr=ptr})

-- | Identical to '>' in BF
moveRight :: VM ()
moveRight = vmState >>= \vm -> moveTo $ (getMemoryPtr vm) + 1

-- | Identical to '<' in BF
moveLeft :: VM ()
moveLeft = vmState >>= \vm -> moveTo $ (getMemoryPtr vm) - 1

-- | Identical to '+' in BF 
increment :: VM ()
increment = getCurrentMem >>= (\val -> setCurrentMem $ val+1)

-- | Identical to '-' in BF 
decrement :: VM ()
decrement = getCurrentMem >>= (\val -> setCurrentMem $ val-1)

-- | Identical to '.' in BF
echo :: VM ()
echo = do 
    vm <- vmState
    val <- getCurrentMem
    setVM (vm{getOutputBuffer=(getOutputBuffer vm) ++ [chr . fromIntegral $ val]})

-- | Identical to ',' in BF
getch :: VM ()
getch = liftIO getChar >>= setCurrentMem . fromIntegral . ord

setInstruction :: Ptr -> VM ()
setInstruction ptr = do
    vm <- vmState
    conf <- vmConf
    let code = getInstructions conf
    if ptr < 0 || ptr >= (VB.length code) then crash "Tried to access unexistent instruction." 
    else setVM (vm {getInstructionPtr = ptr}) 

nextInstruction :: VM ()
nextInstruction = vmState >>= \vm -> setInstruction $ (getInstructionPtr vm) + 1

prevInstruction :: VM ()
prevInstruction = vmState >>= \vm -> setInstruction $ (getInstructionPtr vm) - 1

jumpForward :: Offset -> VM ()
jumpForward offset = do
    vm <- vmState
    let instructionPtr = getInstructionPtr vm
    if offset == 0 then crash "Detected unclosed parenthesis." else return ()
    val <- getCurrentMem
    if val == 0 then setInstruction $ instructionPtr+offset+1
    else nextInstruction

-- | Implements ']' behavior.
jumpBackward :: Offset -> VM ()
jumpBackward offset = do
    vm <- vmState
    let instructionPtr = getInstructionPtr vm
    if offset == 0 then crash "Detected unopened parenthesis." else return ()
    val <- getCurrentMem
    if val == 0 then nextInstruction
    else setInstruction $ instructionPtr - offset

brainfuck :: VM OutputBuffer
brainfuck = do
    vm <- vmState
    conf <- vmConf
    let instructionPtr = getInstructionPtr vm
        code           = getInstructions conf
        instruction    = code VB.! instructionPtr
    case instruction of
        UnknownInstruction char -> nextInstruction >> brainfuck --brainfuck ignores unknown characters
        EOF -> return $ getOutputBuffer vm
        MoveRight -> moveRight >> nextInstruction >> brainfuck
        MoveLeft -> moveLeft >> nextInstruction >> brainfuck
        Increment -> increment >> nextInstruction >> brainfuck
        Decrement -> decrement >> nextInstruction >> brainfuck
        Output -> echo >> nextInstruction >> brainfuck
        Input -> getch >> nextInstruction >> brainfuck
        JumpForward offset -> jumpForward offset >> brainfuck
        JumpBackward offset -> jumpBackward offset >> brainfuck

runVM :: VMConfig -> VM a -> IO (Either RuntimeException a)
runVM conf vm = do 
    let ramCapacity = (getRamCapacity conf)
        ram = VU.replicate ramCapacity 0
        memoryPtr = ramCapacity `div` 2
        initialState = VMState {getMemory=ram, getMemoryPtr=memoryPtr, getInstructionPtr=0, getStack=[], getOutputBuffer=[]}
    result <- runExceptT $ runStateT (runReaderT vm conf) initialState
    case result of
      (Left err) -> return $ Left err
      (Right (val, _)) -> return $ Right val 


stringToCode :: String -> Code
stringToCode [] = VB.singleton EOF
stringToCode ('>':xs) = VB.cons MoveRight $ stringToCode xs 
stringToCode ('<':xs) = VB.cons MoveLeft $ stringToCode xs 
stringToCode ('+':xs) = VB.cons Increment $ stringToCode xs 
stringToCode ('-':xs) = VB.cons Decrement $ stringToCode xs 
stringToCode ('.':xs) = VB.cons Output $ stringToCode xs
stringToCode (',':xs) = VB.cons Input $ stringToCode xs 
stringToCode ('[':xs) = 
    let offset = closureOffset xs 1 0 1 in
        (VB.singleton $ JumpForward offset) VB.++
        (VB.init $ stringToCode $ take (offset-1) xs) VB.++  
        (VB.singleton $ JumpBackward offset) VB.++ 
        (stringToCode $ drop offset xs)
    where
        closureOffset [] _ _ _                = 0
        closureOffset ('[':xs) open close acc = closureOffset xs (open+1) close (acc+1)
        closureOffset (']':xs) open close acc = if open == (close+1) then acc else closureOffset xs open (close+1) (acc+1)
        closureOffset (_:xs) open close acc   = closureOffset xs open close (acc+1)
stringToCode (']':xs) = VB.cons (JumpBackward 0) $ stringToCode xs
stringToCode (x:xs) = VB.cons (UnknownInstruction x) $ stringToCode xs


makeVM :: Capacity -> Capacity -> String -> VMConfig
makeVM mem stack code = VMConfig {getRamCapacity=mem, getStackCapacity=stack, getInstructions=(stringToCode code)}
